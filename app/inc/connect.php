<?php
/* Mit dem Datenbank-Server verbinden, Datenbank wählen und Zeichensatz definieren
- mit Fehlerbehandlung

13.01.2021, Romano Castagnoli
*/


$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
// Verbindung zur Datenbank aufbauen
try{
    $dsn = 'mysql:host=' . $host . ';dbname=' . $database;
    $db = new PDO($dsn, $user, $passwort, $options);
}
// Fehler-Behandlung
catch(PDOExeption $e){
    // Fehlermeldung ohne Details, wird auch im produktiven Web gezeigt
    echo 'verarbeitung fehlgeschlagen';
    // Detaillierte Fehlermeldung, wird nur auf dem Testserver angezeigt (da, wo display_errors auf on gesetzt ist)
    if(ini_get('display errors')){
        echo '<br>'. $e->getMessage();
    }
    // Ausführung des Scripts beenden

    exit;
}


$db -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

?>