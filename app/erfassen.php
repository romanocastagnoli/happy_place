<?php
/*
description: form that loads data into the table tbllernende in the database happy_place
date:   25.02.2021 
author: Romano Castagnoli

*/

//Datenbank-Verbindung aufbauen, Datenbank wählen
require_once("inc/db_inc.php");
require_once("inc/connect.php");

//Ausgabe Header
header('Content-Type text/html; charset=utf-8');


//wenn request vom Formular kommt
if(isset($_POST['save'])){
    $plzID = htmlspecialchars($_POST['plzID']);
    $nachname = htmlspecialchars($_POST['nachname']);
    $vorname = htmlspecialchars($_POST['vorname']);
    
    // SQL-Statement für das Spichern des Datensatzes
    $insertQuery = "INSERT INTO tbllernende (plzIDFK, nachname, vorname) VALUES (:plzID, :nachname, :vorname) ";
    // Prepared Statement
    $prepStat = $db -> prepare($insertQuery);
    // Parameter binden
    $prepStat -> bindParam(':plzID', $plzID);
    $prepStat -> bindParam(':nachname', $nachname);
    $prepStat -> bindParam(':vorname', $vorname);
    // Prepared Statement ausführen (Datensatz speichern)
    $prepStat -> execute();
}
?>
<!DOCTYPE html>
<html lang="de-CH">
<head>
    <meta charset="utf-8">
    <title>Lernenden erfassen</title>
    
<head>
<body>
    <h1>Lernenden erfassen</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" accept-charset="utf-8">
        <label>
            PLZ ID
            <input type="text" name="plzID">
        </label><br>
        <label>
            Nachname
            <input type="text" name="nachname">
        </label><br>
        
        <label>
            Vorname
            <input type="text" name="vorname">
        </label><br>
       
        <label>
            <input type="submit" name="save" value="speichern">
        </label>
    </form>
</body>
</html>