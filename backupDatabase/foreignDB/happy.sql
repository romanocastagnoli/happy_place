﻿drop database if exists happy_place;
create database happy_place;
use happy_place;

create table tblpostleitzahlen (
	plzID bigint(6) unsigned not null auto_increment,
	plz smallint(4) unsigned not null,
	ort varchar(40) not null,
	longitude double signed not null,
	latitude double signed not null,

	primary key(plzID)
);

create table tbllernende (
	id bigint(6) unsigned not null auto_increment,
	plzIDFK bigint(6) unsigned not null,
	nachname	varchar(50) not null,
	vorname		varchar(50) not null,

	primary key(id),
	foreign key(plzIDFK) references tblpostleitzahlen(plzID) on update cascade on delete cascade
);

create table tbladministrator (
	id int(4) unsigned not null auto_increment,
	username varchar(30) not null,
	password varchar(30) not null,
    	primary key(id)
);