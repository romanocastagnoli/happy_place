-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 19. Feb 2021 um 13:34
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `happyplace`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lernende`
--

CREATE TABLE `lernende` (
  `idPK` int(10) NOT NULL,
  `vorname` varchar(45) NOT NULL,
  `nachname` varchar(45) NOT NULL,
  `postleitzahlFK` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ortschaften`
--

CREATE TABLE `ortschaften` (
  `postleitzahlPK` int(10) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `lernende`
--
ALTER TABLE `lernende`
  ADD PRIMARY KEY (`idPK`),
  ADD KEY `postleitzahlFK_index` (`postleitzahlFK`);

--
-- Indizes für die Tabelle `ortschaften`
--
ALTER TABLE `ortschaften`
  ADD PRIMARY KEY (`postleitzahlPK`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `lernende`
--
ALTER TABLE `lernende`
  MODIFY `idPK` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `lernende`
--
ALTER TABLE `lernende`
  ADD CONSTRAINT `lernende_ibfk_1` FOREIGN KEY (`postleitzahlFK`) REFERENCES `ortschaften` (`postleitzahlPK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
