LOAD DATA INFILE 'postleitzahlenMitKoordinaten.csv' 
INTO TABLE tblpostleitzahlen
CHARACTER SET 'latin1'
FIELDS TERMINATED BY ';' 
LINES TERMINATED BY '\n'
(plz, ort, longitude, latitude)
;